#!/bin/bash

DIARY_LOCATION='/home/jfkimmes/Nextcloud/Notes/personal/diary.md'

rm -f /tmp/tmpdiary
touch /tmp/tmpdiary

kitty -o initial_window_width=600 -o initial_window_height=300 -o remember_window_size=no -- nvim /tmp/tmpdiary

if [ -s /tmp/tmpdiary ]; then
    echo "# `date +%D\ %R:%S`" >> $DIARY_LOCATION
    cat /tmp/tmpdiary >> $DIARY_LOCATION
    echo "" >> $DIARY_LOCATION
    echo "---" >> $DIARY_LOCATION
    echo "" >> $DIARY_LOCATION
fi

rm -f /tmp/tmpdiary
