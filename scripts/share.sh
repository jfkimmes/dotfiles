#!/bin/bash

OPTIND=1

get_extension() {
    case $1 in
      (*.*) extension="."${1##*.};;
      (*)   extension="";;
    esac
    echo $extension
}

clear_dir() {
    if [ $1 == "all" ]; then
        ssh xaelrier@brooks.uberspace.de "rm -rf ~/html/share/*"
        echo "Cleared share directory!"
    else
        ssh xaelrier@brooks.uberspace.de "rm -rf ~/html/share/$1"
        echo "Cleared share: $1"
    fi
}

show_help() {
    echo "###### Usage ######"
    echo "-h                      Show this help"
    echo "-p <Path to file>       Path to file"
    echo "-l <Custom Link>        Custom link to upload to"
    echo "-c [all|<custom link>]  Clear all or link"
    echo "-k                      Clipboard input"
}

clipshare() {
    wl-paste > /tmp/clipshare
    target="/tmp/clipshare"
}

if [ $# -lt 1 ]; then
    show_help
fi

# Generate a four word string from EFF Wordlist
randomLocation=`shuf -n 4 ~/dotfiles/resources/wordlist.txt`
randomString=""
for line in $randomLocation; do
    randomString+="${line^}"
done
randomLocation=$randomString

while getopts "hc:p:l:k" opt; do
    case "$opt" in
    h)
        show_help
        exit 0
        ;;
    c)  clear_dir $OPTARG
        ;;
    p)  target=$OPTARG
        ;;
    l)  randomLocation=$OPTARG
        ;;
    k)  clipshare
    esac
done

if [[ -f $target ]]; then
    extension=$(get_extension $target)
    scp $target xaelrier@brooks.uberspace.de:~/html/share/$randomLocation$extension > /dev/null
    if [ $? -ne 0 ]; then
        echo "Error while uploading :("
        exit 1
    fi
    echo "https://share.jfkimmes.eu/$randomLocation$extension"
    echo "https://share.jfkimmes.eu/$randomLocation$extension" | wl-copy
fi
