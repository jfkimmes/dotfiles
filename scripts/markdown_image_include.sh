#!/bin/bash

date=`date +%s`

mkdir -p ~/active-project/images

if [ $XDG_SESSION_TYPE == "x11" ]; then
    xclip -selection c -t image/png -o > ~/active-project/images/$date.png
    echo "![](./images/$date.png)" | xclip -selection c
else
    wl-paste > ~/active-project/images/$date.png
    echo "![](./images/$date.png)" | wl-copy
fi

