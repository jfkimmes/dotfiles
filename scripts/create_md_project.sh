#!/bin/bash

if [ $1 = "" ]; then return 1; fi

cp -r ~/dotfiles/resources/markdown-template ./$1
sed -i "s/#REPLACE_NAME#/$1/" ./$1/Makefile
sed -i "s/#REPLACE_DATE#/$(date +%d.%m.%Y)/" ./$1/template.md

mv ./$1/template.md ./$1/$1.md

