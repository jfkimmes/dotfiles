#!/bin/bash

ENVIRONMENT_HOME=~/.python-venvs

ensure_env_home_exists () {
    mkdir -p $ENVIRONMENT_HOME
}

list () {
    ensure_env_home_exists
    ls $ENVIRONMENT_HOME
}

run () {
    ensure_env_home_exists
    if [ -z ${1+x} ]; then
        echo "Specify env!"
        return
    fi
    source ""$ENVIRONMENT_HOME"/"$1"/bin/activate"
}

create () {
    ensure_env_home_exists
    if [ -z ${1+x} ]; then
        echo "Specify env!"
        return
    fi
    python -m venv "$ENVIRONMENT_HOME"/$1
}

delete () {
    ensure_env_home_exists
    if [ -z ${1+x} ]; then
        echo "Specify env!"
        return
    fi
    rm -r ""$ENVIRONMENT_HOME"/"$1""
}


"$@"
