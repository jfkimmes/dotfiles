# Lines configured by zsh-newuser-install
HISTFILE=~/.histfile
HISTSIZE=1000
SAVEHIST=1000
setopt autocd extendedglob nomatch notify no_auto_remove_slash
unsetopt beep
bindkey -v
# End of lines configured by zsh-newuser-install

source /usr/share/fzf/shell/key-bindings.zsh

# write all commands directly to history file
setopt -o sharehistory

zstyle ':completion:*' matcher-list '' 'm:{a-zA-Z}={A-Za-z}' 'r:|=*' 'l:|=* r:|=*'
autoload -Uz compinit && compinit

# Fix missing backspace in vi edit mode
bindkey "^?" backward-delete-char

# Fix history search
bindkey '^R' history-incremental-pattern-search-backward
bindkey '^B' history-incremental-pattern-search-forward
bindkey -M vicmd "k" history-beginning-search-backward # search history for already entered words backwards in vi mode
bindkey -M vicmd "j" history-beginning-search-forward # search history for already entered words forwards in vi mode
# bindkey -M viins "bla" <do something> # for future reference: insert mode key binding 

# Preferred editor for local and remote sessions
export EDITOR='nvim'
export SUDO_EDITOR='nvim'

# include user base binary directory to path
export PATH=$PATH:${HOME}/.local
export PATH=$PATH:${HOME}/.local/bin

### Aliases ############################
alias vim=nvim
alias tm='tmux new-session -A -s main'
alias xclipc='xclip -selection c'
alias xopen='xdg-open'

# Scripts
alias diary='~/dotfiles/scripts/diary.sh'

alias sharelink='~/dotfiles/scripts/share.sh'

alias mdnote='~/dotfiles/scripts/create_md_project.sh'
alias mdimg='~/dotfiles/scripts/markdown_image_include.sh'
alias ltimg='~/dotfiles/scripts/latex_image_include.sh'
alias activate='~/dotfiles/scripts/activate_project.sh'

alias pyenv='. ~/dotfiles/scripts/pythons_env_manager.sh'

# Tools
alias wifi='qrencode -t utf8 "WIFI:S:LAN Solo Guest;T:WPA;P:`pass router/wifi_guest`;;"; echo `pass router/wifi_guest`'
alias hack='cmatrix -BC green'

# Docker
alias hpodman='flatpak-spawn --host podman'
alias docker='flatpak-spawn --host podman'
export DOCKER_HOST=unix:///run/user/$UID/podman/podman.sock
### Functions ############################


function wttr() {
    curl wttr.in/$1
}

function cheat() {
    curl cheat.sh/$1 | less -R
}

function cl() {
    cd $@ && ls --color=always -l
}

function stream() {
    streamlink "$1" best --player "flatpak-spawn --host flatpak run org.videolan.VLC"
}

### Plugins ############################
# Auto Suggestions
source ~/dotfiles/plugins/zsh-autosuggestions/zsh-autosuggestions.zsh

# Syntax Highlighting
source ~/dotfiles/plugins/zsh-syntax-highlighting/zsh-syntax-highlighting.zsh

# Pure Prompt
fpath+=~/dotfiles/plugins/zfunctions/pure
autoload -U promptinit; promptinit
prompt pure

# activate `direnv` tool for per directory/project env variables
if [ "$HOST" = "toolbox" ]; then
    eval "$(direnv hook zsh)"
fi
