# Dotfiles

## Usage
To use these dotfiles clone the repo into a subdirectory of your home directory.  
Get the themes and plugins with:  
```
git submodule init
git submodule update
```

### Sourcing 
Backup your old dotfiles and replace them with new ones sourcing from the dotfiles directory, like this:

```
# ~/.zshrc
source ~/dotfiles/zshrc
```
```
# ~/.config/nvim/init.vim
so ~/dotfiles/init.vim
```
```
# ~/.tmux.conf
source ~/dotfiles/tmux.conf
```

