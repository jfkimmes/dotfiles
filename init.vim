" Plugins will be downloaded under the specified directory.
call plug#begin(has('nvim') ? stdpath('data') . '/plugged' : '~/.vim/plugged')

" ### vim plugins ###
" Navigation
Plug 'ctrlpvim/ctrlp.vim'
Plug 'preservim/nerdtree'

" IDE
Plug 'neoclide/coc.nvim', {'branch': 'release'}
Plug 'github/copilot.vim'
Plug 'junegunn/fzf', { 'do': { -> fzf#install() } }
Plug 'junegunn/fzf.vim'


" Theme
Plug 'dracula/vim', { 'as': 'dracula' }
call plug#end()

" -----------------------------------------------------------------------------

" Custom leader shortcuts
let mapleader = ' '
noremap <leader><leader> <C-^>
"
" General indentation settings
set tabstop=4 softtabstop=4 shiftwidth=4 expandtab

" Textfiles, Latex
au BufNewFile,BufRead *.txt set linebreak
au BufNewFile,BufRead *.tex set linebreak
 
" Markdown
au BufNewFile,BufRead *.md set linebreak
au BufNewFile,BufReadPost *.md set filetype=markdown
let g:markdown_fenced_languages = ['bash=sh', 'css', 'django', 'javascript', 'js=javascript', 'json=javascript', 'php', 'python', 'xml', 'html', 'c', 'cpp', 'rust']

au BufNewFile,BufRead *.hbs set filetype=html
au BufNewFile,BufRead *.jinja2 set filetype=html

" General Appearance
colorscheme dracula
hi Normal guibg=#1e1f29    " overwrites background to terminal bg color
set termguicolors
set t_Co=256
syntax on

" Behaviour
"set cursorline
set nu
set relativenumber
set hidden              " allows hidden buffers without saving
set wrap                " visually wrap lines
set linebreak           " only wrap at linebreak characters
set mouse=a             " Mouse Mode
set splitbelow          " Sane split defaults
set splitright          " Sane split defaults
set exrc                " Allow local project-wise .vimrc's - keep `set secure` for this!


" Search
set incsearch           " search as characters are entered
set hlsearch            " highlight matches
set ignorecase


" Undo History

set undofile                                 " tell it to use an undo file
set undodir=$HOME/.config/nvim/undo          " set a directory to store the undo history

" Easy Copy - Paste
vmap <C-c> "+yi
vmap <C-x> "+c
vmap <C-v> c<ESC>"+p
imap <C-v> <C-r><C-o>+


" Fulltext Search
noremap <leader>f :Rg<Return>


" ~~~ CtrlP ~~~
" Add additional root markers for CtrlP (tells CtrlP where project root is)
let g:ctrlp_root_markers = ['Pipfile', 'Cargo.toml', 'pyproject.toml', 'build.gradle']

" Set working directory for CtrlP
" - r: nearest ancestor of current file that contains .git OR root_markers
" - a: current file's dir unless it's a subdirectory of CWD
let g:ctrlp_working_path_mode = 'ra'

" Ignore .git dir and files in .gitignore, when searching with CtrlP
let g:ctrlp_user_command = ['.git/', 'git --git-dir=%s/.git ls-files -oc --exclude-standard']

" Ignorelist
set wildignore+=*.swp,*/target/*

" ~~~ Copilot ~~~
imap <silent><script><expr> <C-Y> copilot#Accept("\<CR>")
        let g:copilot_no_tab_map = v:true


" ~~~ CoC ~~~
" Some servers have issues with backup files, see #649
set nobackup
set nowritebackup

" Having longer updatetime (default is 4000 ms = 4s) leads to noticeable
" delays and poor user experience
set updatetime=300

" Always show the signcolumn, otherwise it would shift the text each time
" diagnostics appear/become resolved
set signcolumn=yes

nmap <silent> gd <Plug>(coc-definition)
nmap <silent> gy <Plug>(coc-type-definition)
nmap <silent> gi <Plug>(coc-implementation)
nmap <silent> gr <Plug>(coc-references)
autocmd FileType python let b:coc_root_patterns = ['Pipfile.lock', 'pyproject.toml', '.git', '.env']

" Use <c-space> to trigger completion
if has('nvim')
  inoremap <silent><expr> <C-space> coc#refresh()
else
  inoremap <silent><expr> <C-@> coc#refresh()
endif

" Symbol renaming
nmap <leader>rn <Plug>(coc-rename)

" Use K to show documentation in preview window
nnoremap <silent> K :call ShowDocumentation()<CR>
function! ShowDocumentation()
  if CocAction('hasProvider', 'hover')
    call CocActionAsync('doHover')
  else
    call feedkeys('K', 'in')
  endif
endfunction

" Use `[g` and `]g` to navigate diagnostics
" Use `:CocDiagnostics` to get all diagnostics of current buffer in location list
nmap <silent> [g <Plug>(coc-diagnostic-prev)
nmap <silent> ]g <Plug>(coc-diagnostic-next)

" Add `:Format` command to format current buffer
command! -nargs=0 Format :call CocActionAsync('format')

" Add `:OR` command for organize imports of the current buffer
command! -nargs=0 OR   :call     CocActionAsync('runCommand', 'editor.action.organizeImport')

" ~~~ Nerdtree ~~~
map <C-n> :NERDTreeToggle<CR>

" ~~~ Security ~~~
set nomodeline
set secure
